;;; publish.el --- Export Org mode files to HTML and tangle code

;; Author: Ben Zwick
;; Created: 26 July 2018

;;; Commentary:
;;
;; Run the script from a terminal:
;;
;;   emacs --script publish.el
;;
;; to recursively export and tangle the .org files
;; in the current directory.

(package-initialize)

(require 'org)
(require 'ox-extra)

(ox-extras-activate '(ignore-headlines))

(setq org-src-preserve-indentation t)

(setq org-publish-project-alist
      '(("html"
         :base-directory "./"
         :base-extension "org"
         :recursive t
         :publishing-directory "./publish/html/"
         :with-toc nil
         :publishing-function org-html-publish-to-html)
        ("scripts"
         :base-directory "./"
         :base-extension "org"
         :publishing-directory "./publish/scripts/"
         :recursive t
         :publishing-function org-babel-tangle-publish)))

(org-publish "html" t)
(org-publish "scripts" t)
